import { Dirname } from '@nightspade/bundler/lib/config'
import { Config } from '@nightspade/bundler';

/// Do not adding or editing outside marker bellow
/// CUSTOM PRE CONFIG START

/// CUSTOM PRE CONFIG END

const config = Config(
    Dirname(__dirname),
/// Do not adding or editing outside marker bellow
/// CUSTOM CONFIG START
    
/// CUSTOM CONFIG END
);

/// Do not adding or editing outside marker bellow
/// CUSTOM POST CONFIG START

/// CUSTOM POST CONFIG END

export default config;