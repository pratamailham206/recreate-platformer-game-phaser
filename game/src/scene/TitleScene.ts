import $background from '@Assets/background/parallax-mountain.png';
import $playBtn from '@Assets/ui/PLAY.png';
import $creaditBtn from '@Assets/ui/credit.png';
import $bgm from '@Assets/sound/bgm.ogg';

import { SceneID } from '../sceneID';
import { BaseScene, ChangeScene } from '@nightspade/core3/lib/scene';
import { AddButton, Button } from '@nightspade/core3/lib/ui';
import { RegisterAsset } from '@nightspade/core3/lib/loader';
import { GetCenterX, GetCenterY } from '@nightspade/core3/lib/config';
import { AddText } from '@nightspade/core3/lib/gameObject';
import { PlayBGM } from '@nightspade/core3/lib/audio/BGM';

export class TitleScene extends BaseScene<undefined> {
    constructor() {
        super(SceneID.TITLE);

        RegisterAsset(this, $background, true);
        RegisterAsset(this, $playBtn, true);
        RegisterAsset(this, $creaditBtn, true);
        RegisterAsset(this, $bgm, true);
    }

    create() {        
        //BGM
        PlayBGM($bgm);

        this.add.image(GetCenterX(), GetCenterY(), $background.key, $background.frame).setScale(1.68);      
        AddText(this,"COLLECT THE DIAMONDS", 120,120, {font: "60px Arial", color: "#000"});
        AddButton(this, $playBtn, GetCenterX(), GetCenterY()).setScale(0.6).on(Button.Events.CLICK, () => {
            ChangeScene(this, SceneID.GAME, undefined, true);   
        });
        AddButton(this, $creaditBtn, GetCenterX(), GetCenterY() + 100).setScale(0.3).on(Button.Events.CLICK, () => {
            ChangeScene(this, SceneID.CREDIT, undefined, true);   
        });
    }

}