//ASSETS
import $background from '@Assets/background/pabrik.png';
import $ground from '@Assets/env/magic-cliffs.png';
import $diamonds from '@Assets/env/diamond.png';
import $bomb from '@Assets/env/bomb.png';
import $woof from '@Assets/character/woof.png';
import $sfx from '@Assets/sound/sfx.mp3';
import $level1 from '@Assets/Level/level1.json';

//GAMEOBJECT
import Player from '../gameobjects/Player';
import Diamond from '../gameobjects/Diamond';
import Bombs from '../gameobjects/Bomb';

import { SceneID } from '../sceneID';
import { Input } from 'phaser';
import { BaseScene, ChangeScene } from '@nightspade/core3/lib/scene';
import { GetCenterX, GetCenterY, GetSafeHeight, GetSafeTop, GetWidth } from '@nightspade/core3/lib/config';
import { AddText } from '@nightspade/core3/lib/gameObject';
import { RegisterAsset } from '@nightspade/core3/lib/loader';
import { PlaySFX } from '@nightspade/core3/lib/audio/SFX';

export class LevelScene extends BaseScene<undefined> {
    constructor() {
        super(SceneID.LEVEL);

        RegisterAsset(this, $background, true);
        RegisterAsset(this, $ground, true);
        RegisterAsset(this, $diamonds, true);
        RegisterAsset(this, $bomb, true);
        RegisterAsset(this, $woof, true);
        RegisterAsset(this, $sfx);                
        RegisterAsset(this, $level1, true);
    }

    preload() {
        super.preload();
        this.load.spritesheet('woof', $woof.url, { frameWidth: 32, frameHeight: 32 });
    }    

    create(config?: object) {  
        const thisScene = this; 

        let level = Object(config)["levelData"];
        var background = Object(config)["background"];
        var levelData = this.cache.json.get(level);
        
        //BACKGROUND
        this.add.image(GetCenterX(), GetCenterY(), background.key, background.frame).setScale(1);

        //SCORE
        var score = 0;
        const TextScore = AddText(this,'score: 0', GetCenterX() - 100, GetCenterY() - 330, { fontSize: '32px', color: '#FFFFFF' });

        //GROUND
        const platforms = this.physics.add.staticGroup();
        platforms.create(levelData.platforms[0].x, levelData.platforms[0].y, $ground.key).setScale(1).refreshBody();
        platforms.create(levelData.platforms[1].x, levelData.platforms[1].y, $ground.key);
        platforms.create(levelData.platforms[2].x, levelData.platforms[2].y, $ground.key);
        platforms.create(levelData.platforms[3].x, levelData.platforms[3].y,  $ground.key);
        platforms.create(levelData.platforms[4].x, levelData.platforms[4].y,  $ground.key);
        platforms.create(levelData.platforms[5].x, levelData.platforms[5].y,  $ground.key).setScale(2).refreshBody();    

        //DIAMOND
        const diamonds = new Diamond(this.physics.world ,this, levelData.diamond.x, levelData.diamond.y, levelData.diamond.step, $diamonds.key);
        
        function getDiamond(player: any, diamond: any)
        {   
            PlaySFX($sfx);
            diamond.disableBody(true, true);
            score += 10;            
            TextScore.setText("Score :" + score);
            //IF DIAMOND EMPTY
            if (diamonds.countActive(true) === 1)
            {
                diamonds.Spawn();
                diamonds.bounceAnim(thisScene);
            }
            //CREATE BOMB
            if (bombs.countActive(true) < levelData.bomb.maxBomb) {
                var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
                var bomb = bombs.create(x, levelData.bomb.y, $bomb.key);
                bomb.setBounce(0.75);
                bomb.setCollideWorldBounds(true);
                bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);       
            }
            checkWin();
        }

        //PLAYER
        const player = new Player(this, levelData.playerStart.x, levelData.playerStart.y);

        //BOMB
        var bombs = new Bombs(this.physics.world, this);

        function hitBomb (player: any, bomb: any)
        {   
            score -= 1;            
            TextScore.setText("Score :" + score);
            player.anims.play('turn');
        }

        this.physics.add.collider(bombs, platforms);
        this.physics.add.collider(player, bombs, hitBomb);

        //COLLIDER & COLLISION
        this.physics.add.collider(player, platforms);
        this.physics.add.collider(diamonds, platforms);
        this.physics.add.overlap(player, diamonds, getDiamond);

        //INITIALIZE PHYSICS
        if(this.physics) {    
            this.physics.world.setBounds(0, GetSafeTop(), GetWidth(), GetSafeHeight());
            this.physics.world.gravity.y = 980;
        }

        //KEYBOARD INPUT
        this.input.keyboard.addKey(Input.Keyboard.KeyCodes.W)
        .setEmitOnRepeat(true)
        .on(Input.Keyboard.Events.DOWN, () => {
            player.jump();
        });

        this.input.keyboard.addKey(Input.Keyboard.KeyCodes.D)
        .setEmitOnRepeat(true)
        .on(Input.Keyboard.Events.DOWN, () => {
            player.moveRight();
        })
        .on(Input.Keyboard.Events.UP, () => {
           player.idle();
        });

        this.input.keyboard.addKey(Input.Keyboard.KeyCodes.A)
        .setEmitOnRepeat(true)
        .on(Input.Keyboard.Events.DOWN, () => {
            player.moveLeft();
        })
        .on(Input.Keyboard.Events.UP, () => {
            player.idle();
        });

        function checkWin() {
            if(score >= levelData.status.scoreToWin) {
                ChangeScene(thisScene, SceneID.SCORE, { 
                    playerScore: score,
                    nextLevel: levelData.id,
                }, true);
                score = 0;
            }
        }
    }
}