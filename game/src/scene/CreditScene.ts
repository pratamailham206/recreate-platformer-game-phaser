import $background from '@Assets/background/Background.png';
import $back from '@Assets/ui/back.png';

import { SceneID } from '../sceneID';
import { BaseScene, ChangeScene } from '@nightspade/core3/lib/scene';
import { GetCenterX, GetCenterY, Scene } from '@nightspade/core3/lib/config';
import { AddText } from '@nightspade/core3/lib/gameObject';
import { RegisterAsset } from '@nightspade/core3/lib/loader';
import { Button, AddButton } from '@nightspade/core3/lib/ui';

export class CreditScene extends BaseScene<undefined> {
    constructor() {
        super(SceneID.CREDIT);

        RegisterAsset(this, $background, true);
        RegisterAsset(this, $back, true);
    }    

    create() {
        const background = this.add.image(GetCenterX(), GetCenterY(), $background.key, $background.frame).setScale(1.4);
        const creditTxt = AddText(this, "< - Credit - >", GetCenterX() - 135, GetCenterY() - 180, {font: '48px Arial', color: "black"});
        const text = AddText(this, "created using phaser framework", GetCenterX() - 170, GetCenterY() - 80, {font: '24px Arial', color: 'black'});

        const creditButton = AddButton(this, $back, 100, 100).setScale(0.3).on(Button.Events.CLICK, () => {
            ChangeScene(this, SceneID.TITLE, undefined, true);
        }); 
    }
}