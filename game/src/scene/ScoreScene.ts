import $sfx from '@Assets/sound/sfx.mp3';
import $background from '@Assets/background/pabrik.png';
import $back from '@Assets/ui/Btm.png';
import $next from '@Assets/ui/Nl.png';

import $level1 from '@Assets/Level/level1.json';
import $background1 from '@Assets/background/pabrik.png';
import $level2 from '@Assets/Level/level2.json';
import $background2 from '@Assets/background/flat-night.png';
import $level3 from '@Assets/Level/level3.json';
import $background3 from '@Assets/background/Reference.png';
import $level4 from '@Assets/Level/level4.json';
import $background4 from '@Assets/background/Reference-Image.png';
import $level5 from '@Assets/Level/level5.json';
import $background5 from '@Assets/background/Layer1.png';
import $level6 from '@Assets/Level/level6.json';
import $background6 from '@Assets/background/forest-mist.png';
import $level7 from '@Assets/Level/level7.json';
import $background7 from '@Assets/background/Reference-Image.png';
import $level8 from '@Assets/Level/level8.json';
import $background8 from '@Assets/background/flat-night.png';

import { SceneID } from '../sceneID';
import { BaseScene, ChangeScene } from '@nightspade/core3/lib/scene';
import { GetCenterX, GetCenterY } from '@nightspade/core3/lib/config';
import { AddText } from '@nightspade/core3/lib/gameObject';
import { AddButton, Button } from '@nightspade/core3/lib/ui';
import { RegisterAsset } from '@nightspade/core3/lib/loader';
import { PlaySFX } from '@nightspade/core3/lib/audio/SFX';

export class ScoreScene extends BaseScene<undefined> {
    constructor() {
        super(SceneID.SCORE);

        RegisterAsset(this, $background, true);
        RegisterAsset(this, $back, true);
        RegisterAsset(this, $next, true);
        RegisterAsset(this, $sfx);   

        RegisterAsset(this, $level1, true);
        RegisterAsset(this, $background1, true);
        RegisterAsset(this, $level2, true);
        RegisterAsset(this, $background2, true);
        RegisterAsset(this, $level3, true);
        RegisterAsset(this, $background3, true);
        RegisterAsset(this, $level4, true);
        RegisterAsset(this, $background4, true);
        RegisterAsset(this, $level5, true);
        RegisterAsset(this, $background5, true);
        RegisterAsset(this, $level6, true);
        RegisterAsset(this, $background6, true);
        RegisterAsset(this, $level7, true);
        RegisterAsset(this, $background7, true);
        RegisterAsset(this, $level8, true);
        RegisterAsset(this, $background8, true);
    }

    create(config?: object) {        
        var level = [$level1, $level2, $level3, $level4, $level5, $level6, $level7, $level8];
        var backdrop = [$background1, $background2, $background3, $background4, $background5, $background6, $background7, $background8];

        let score = Object(config)["playerScore"];
        let id = Object(config)["nextLevel"];

        if(id == 8) {
            id = 0;
        }
   
        this.add.image(GetCenterX(), GetCenterY(), backdrop[id].key, backdrop[id].frame).setScale(1);
        AddText(this, "YOU WIN", GetCenterX() - 150, 100, {font: '64px Arial', color: "white"});
        AddText(this, "Your Score: " + score, 350, 200, {font: '42px Arial', color: 'white'});

        AddText(this, "Continue", 425, 400, {font: '36px Arial', color: 'white'});
        
        AddButton(this, $next, 325, 500).setScale(0.3).on(Button.Events.CLICK, () => {
            PlaySFX($sfx);
            ChangeScene(this, SceneID.LEVEL, {                         
                levelData: level[id],
                background: backdrop[id]
            }, true);   
        });
        AddButton(this, $back, 670, 500).setScale(0.3).on(Button.Events.CLICK, () => {
            PlaySFX($sfx);
            ChangeScene(this, SceneID.TITLE, undefined, true);   
        });
    }
}