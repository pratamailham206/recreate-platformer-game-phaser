//ASSETS
import $background from '@Assets/background/pabrik.png';
import $sfx from '@Assets/sound/sfx.mp3';
import $button from '@Assets/ui/template.png';

import $level1 from '@Assets/Level/level1.json';
import $background1 from '@Assets/background/pabrik.png';
import $level2 from '@Assets/Level/level2.json';
import $background2 from '@Assets/background/flat-night.png';
import $level3 from '@Assets/Level/level3.json';
import $background3 from '@Assets/background/Reference.png';
import $level4 from '@Assets/Level/level4.json';
import $background4 from '@Assets/background/Reference-Image.png';
import $level5 from '@Assets/Level/level5.json';
import $background5 from '@Assets/background/Layer1.png';
import $level6 from '@Assets/Level/level6.json';
import $background6 from '@Assets/background/forest-mist.png';
import $level7 from '@Assets/Level/level7.json';
import $background7 from '@Assets/background/Reference-Image.png';
import $level8 from '@Assets/Level/level8.json';
import $background8 from '@Assets/background/flat-night.png';

import { SceneID } from '../sceneID';
import { BaseScene, ChangeScene } from '@nightspade/core3/lib/scene';
import { AddButton, Button } from '@nightspade/core3/lib/ui';
import { GetCenterX, GetCenterY } from '@nightspade/core3/lib/config';
import { AddText } from '@nightspade/core3/lib/gameObject';
import { RegisterAsset } from '@nightspade/core3/lib/loader';
import { PlaySFX } from '@nightspade/core3/lib/audio/SFX'

export class GameScene extends BaseScene<undefined> {
    constructor() {
        super(SceneID.GAME);

        RegisterAsset(this, $background, true);
        RegisterAsset(this, $button, true);
        RegisterAsset(this, $sfx);   

        RegisterAsset(this, $level1, true);
        RegisterAsset(this, $background1, true);
        RegisterAsset(this, $level2, true);
        RegisterAsset(this, $background2, true);
        RegisterAsset(this, $level3, true);
        RegisterAsset(this, $background3, true);
        RegisterAsset(this, $level4, true);
        RegisterAsset(this, $background4, true);
        RegisterAsset(this, $level5, true);
        RegisterAsset(this, $background5, true);
        RegisterAsset(this, $level6, true);
        RegisterAsset(this, $background6, true);
        RegisterAsset(this, $level7, true);
        RegisterAsset(this, $background7, true);
        RegisterAsset(this, $level8, true);
        RegisterAsset(this, $background8, true);
    }

    preload() {
        super.preload();
    }    

    create() {        
        //BACKGROUND
        this.add.image(GetCenterX(), GetCenterY(), $background.key, $background.frame).setScale(1);
        var level = [$level1, $level2, $level3, $level4, $level5, $level6, $level7, $level8];
        var backdrop = [$background1, $background2, $background3, $background4, $background5, $background6, $background7, $background8];

        for(let i = 0; i < level.length; i++) {  
            if(i >= 4) {
                AddButton(this, $button, GetCenterX() - 300 + (i-4) * 200, GetCenterY() + 100).setText(`${i + 1}`, { fontSize: "64px Arial", color: "black"}).on(Button.Events.CLICK, () => {
                    PlaySFX($sfx);
                    ChangeScene(this, SceneID.LEVEL, {                         
                        levelData: level[i],
                        background: backdrop[i]
                     }, true);   
                });
            }       
            else {
                AddButton(this, $button, GetCenterX() - 300 + i * 200, GetCenterY() - 100).setText(`${i + 1}`, { fontSize: "64px Arial", color: "black"}).on(Button.Events.CLICK, () => {
                    PlaySFX($sfx);
                    ChangeScene(this, SceneID.LEVEL, { 
                        levelData: level[i],
                        background: backdrop[i]
                     }, true);   
                });
            }               
        }

        //TEXT
        AddText(this, "Select Level", GetCenterX() - 120, GetCenterY() - 330, { fontSize: '32px', color: '#FFFFFF' }).setOrigin(0);
    }
}