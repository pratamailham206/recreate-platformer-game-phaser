import Phaser from 'phaser';    

export default class Diamond extends Phaser.Physics.Arcade.Group {
    constructor(world: Phaser.Physics.Arcade.World, scene: Phaser.Scene, x: number, y: number, step: number, key: string) {
        super(world, scene, {
            key: key,
            repeat: 12,
            setXY: { x: x, y: y, stepX: step }
        });

        this.bounceAnim(scene);
    }

    public Spawn() {
        this.children.iterate(function (child: any) {
            child.enableBody(true, child.x, 300, true, true);
        });
    }

    public bounceAnim(scene: Phaser.Scene) {
        this.children.iterate(function (child) {
            child.arcadeBody.setBounceY(Phaser.Math.FloatBetween(0.6, 0.9)); 
            child.arcadeBody.setCollideWorldBounds();
            child.arcadeBody.setVelocity(Phaser.Math.FloatBetween(-50, 50), 20);
            scene.time.addEvent({
                callback: () => {
                    child.arcadeBody.setVelocity(Phaser.Math.FloatBetween(-1, 1), 20);
                },
                callbackScope: scene,
                delay: Phaser.Math.FloatBetween(1500, 2500),
                loop: false
            });
                    
            scene.add.tween({
                duration: Phaser.Math.FloatBetween(1000, 2000),
                targets: child,
                scale: Phaser.Math.FloatBetween(0.4,0.8),
                ease: Phaser.Math.Easing.Bounce.Out,
                yoyo: true,
                loop: true,
            });
        });
    }
}