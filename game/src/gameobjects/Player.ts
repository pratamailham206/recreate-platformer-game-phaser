import Phaser from 'phaser';

export default class Player extends Phaser.GameObjects.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number)
	{
        super(scene, x, y, 'woof');
        scene.add.existing(this);

        //PHYSICS
        scene.physics.add.existing(this);
        this.arcadeBody.setBounceY(0.2);
        this.arcadeBody.setCollideWorldBounds();

        //ANIMATION
        scene.anims.create({
            key: 'left',
            frames: scene.anims.generateFrameNumbers('woof', { start: 0, end: 1 }),
            frameRate: 10,
            repeat: -1
        });
        
        scene.anims.create({
            key: 'turn',
            frames: [ { key: 'woof', frame: 2 } ],
            frameRate: 20
        });
        
        scene.anims.create({
            key: 'right',
            frames: scene.anims.generateFrameNumbers('woof', { start: 2, end: 3 }),
            frameRate: 10,
            repeat: -1
        });
    }

    public jump() {
        if(this.arcadeBody.touching.down) {
            this.arcadeBody.setVelocityY(-600);
        }
    }

    public moveLeft() {
        this.arcadeBody.setVelocityX(-160);                
        this.anims.play('left', true);
    }

    public moveRight() {
        this.arcadeBody.setVelocityX(160);                
        this.anims.play('right', true);
    }

    public idle() {
        this.arcadeBody.setVelocityX(0);                
        this.anims.play('turn');
    }
}