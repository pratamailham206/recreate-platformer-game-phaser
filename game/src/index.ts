import { Game } from '@nightspade/core3/lib/Game';
import { BackgroundColor, Size, ScaleClip, Scenes, ArcadePhysics, DOM } from '@nightspade/core3/lib/config';
import { TitleScene } from './scene/TitleScene';
import { GameScene } from './scene/GameScene';
import { CreditScene } from './scene/CreditScene';
import { ScoreScene } from './scene/ScoreScene';
import { LevelScene } from './scene/LevelScene';
import { DefaultInitialLoadingScene, DefaultLoadingScene } from '@nightspade/core3/lib/scene';

const game = Game(
    BackgroundColor(0x000000),
    Size(1024, 720),
    ScaleClip(1024, 720),
    // ScaleHide(512 + 2 * 160, 1024 + 2 * 64),
    // ScaleNone(),
    Scenes(DefaultInitialLoadingScene, TitleScene, GameScene, DefaultLoadingScene, CreditScene, ScoreScene,
        LevelScene),
    // Modals(ClassOfModal, AnotherModal), // Similar to `Scenes` above. Modal extends `BaseModal` class.
    ArcadePhysics(),
    // MatterPhysics(),
    DOM({
        createContainer: true,
    }),
    // Render({
    // }),
);