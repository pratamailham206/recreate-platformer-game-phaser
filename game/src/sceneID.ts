import { CoreSceneID } from "@nightspade/core3/lib/scene";

export const SceneID = {
    TITLE: CoreSceneID.__LANDING__,
    GAME: 'GAME',
    CREDIT: 'CREDIT',
    SCORE: 'SCORE',
    LEVEL: 'LEVEL'
}